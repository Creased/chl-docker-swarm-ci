# Mise en place d'une infrastructure d'intégration continue avec Docker Swarm et GitLab-CI

 - [Debian Jessie](BASE.md)&nbsp;: Installation de Debian Jessie&nbsp;;
 - [DNS](DNS.md)&nbsp;: Installation d'un service DNS (factultatif si un service existe déjà)&nbsp;;
 - [Docker](DOCKER.md)&nbsp;: Installation et configuration de Docker-CE sur Debian Jessie (sur tous les noeuds)&nbsp;;
 - [Docker Swarm](SWARM.md)&nbsp;: Mise en place d'un dispositif d'orchestration de services avec Docker Swarm&nbsp;;
 - [Docker Registry](REGISTRY.md)&nbsp;: Mise en place d'un registre privé d'images Docker&nbsp;;
 - [Hooked On](WEBHOOK.md)&nbsp;: Mise en place d'un dispositif pour le déploiement continu&nbsp;;
 - [GitLab Runner](GITLAB_RUNNER.md)&nbsp;: Mise en place d'un runner pour GitLab-CI.

Le réseau type pour mettre en place cette infrastructure peut se schématiser commme suit&nbsp;:

![Docker Lab](illustrations/docker_lab.png)

Le serveur de versioning GitLab et son système d'intégration continue peuvent ici être dans le Cloud ou placés sur le même réseau que le Docker Swarm.

Le runner de GitLab-CI devra en revanche être sur le même réseau ou pouvoir y accéder (*port mapping*, routage, etc.) afin de pouvoir déployer le service et stocker l'image sur le registre d'image privé.

Une fois mis en oeuvre, chaque hôte du Docker Swarm exécutera un service supplémentaire spécifique à son dimensionnement et à son rôle, ici&nbsp;:

| Hôte     | Rôle    | Service supplémentaire                                            |
|----------|---------|-------------------------------------------------------------------|
| manager1 | Manager | [Hooked-on](https://gitlab.miletrie.chl/docker/cas-hooked-on)     |
| manager2 | Manager | [Hooked-on](https://gitlab.miletrie.chl/docker/cas-hooked-on)     |
| node1    | Worker  | [GitLab-Runner](https://gitlab.miletrie.chl/docker/gitlab-runner) |
| node2    | Worker  | [GitLab-Runner](https://gitlab.miletrie.chl/docker/gitlab-runner) |

Ansi, pour un projet donné, chacune des entités aura en charge le traitement d'une tâche du Pipeline (en général, la majorité des actions se situent au niveau des workers), spécifique à son rôle ou au service qu'elle héberge, avec un équilibrage de charges assuré par la redondance des hôtes et rôles&nbsp;:

![Pipeline des test](illustrations/gitlab-ci_job_57.png)

![Répartition automatique des charges et tâches](illustrations/swarm_ci_job_57.png)
