# Mise en place d'un dispositif pour le déploiement continu

## Information

Docker Swarm et GitLab-CI étant deux composants distincts du pipeline CI/CD, il est nécessaire de disposer d'une liaison entre ces deux groupements d'entités.

Avec hooked-on, il est possible de configurer des crochets (*WEB hooks*) permettant, par exemple, de mettre à jour les services Docker avec une simple requête HTTP.

Afin de privatiser cette fonctionnalité, il est également possible d'intégrer un ou plusieurs tokens qui devront être indiqués lors de la requête vers ce crochet (*hook triggering*) (voir le fichier config.json).

## Installation (sur tous les masters)

Installation de `node`, `npm` et `nodemon`&nbsp;:

```bash
wget https://nodejs.org/dist/v6.10.3/node-v6.10.3-linux-x64.tar.gz -O /tmp/node.tar.gz
tar xvzf /tmp/node.tar.gz --strip-components=1 -C /usr/local
apt-get -fy install npm
npm install -g nodemon

```

Téléchargement du projet&nbsp;:

```bash
git clone https://gitlab.miletrie.chl/docker/cas-hooked-on /opt/hooked-on/
pushd /opt/hooked-on/
chmod o+w /opt/hooked-on/logs/
install -o user -g user -m 755 -d /opt/services

```

Installation des dépendances&nbsp;:

```bash
npm install

```

Création d'un service pour `systemd` pour démarrer le serveur à chaque démarrage&nbsp;:

```bash
cat <<-'EOF' >/etc/default/hooked-on
##
# Location of project
#
PROJECT_PATH=/opt/hooked-on

##
# General daemon startup options
#
CMD="start"

EOF

cat <<-'EOF' >/etc/systemd/system/hooked-on.service
[Unit]
Description=Simple WEB hooks triggering API

[Service]
User=root
WorkingDirectory=/opt/hooked-on
ExecStart=/usr/bin/npm start
Type=simple

[Install]
WantedBy=basic.target

EOF

systemctl enable hooked-on.service
systemctl start hooked-on.service

mkdir -p /etc/systemd/system/hooked-on.service.d
cat <<-'EOF' >/etc/systemd/system/hooked-on.service.d/exec.conf
[Service]
EnvironmentFile=-/etc/default/hooked-on

# Reset
User=
WorkingDirectory=
ExecStart=

# Define
User=user
WorkingDirectory=${PROJECT_PATH:-/opt/hooked-on}
ExecStart=/usr/bin/npm $CMD

EOF
systemctl --system daemon-reload

```

Redémarrage du service&nbsp;:

```bash
systemctl restart hooked-on.service

```

Affichage du status et des journaux associés au service&nbsp;:

```bash
systemctl status hooked-on.service -l
journalctl -f -u hooked-on.service

```

## Test manuel

Une fois installé sur les deux managers, on peut vérifier le fonctionnement du hook manuellement avant de l'intégrer au pipeline&nbsp;:

```bash
DATA=$(curl --silent -w "\n%{http_code}" --request "GET" \
     --url "http://manager.docker.lab:8080/update?token=fmDCNtKmVTq8orDtWb3O2SUg8wKald8OjRMPHXQyxv7J5arixKUxUEMdSL4KDOaX")
CODE=$(echo "${DATA}" | tail -n-1)
DATA=$(echo "${DATA}" | head -n-1)

case "${CODE}" in
    20[0-9])
        PROMPT=$'\033[1;32m[+]'
    ;;
    30[0-9])
        PROMPT=$'\033[1;36m[*]'
    ;;
    40[0-9])
        PROMPT=$'\033[1;33m[?]'
    ;;
    * | 5[0-9])
        PROMPT=$'\033[1;31m[!]'
    ;;
esac

export PROMPT DATA

python -c "
import sys, json, os;
data = os.environ['DATA'].decode('utf-8').encode('ascii','xmlcharrefreplace');
data = json.loads(data);
print '{prompt}\033[0m {status}\n\n{output}'.format(prompt=os.environ['PROMPT'], status=data['status'], output=data['output'])"
```
