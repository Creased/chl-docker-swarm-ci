# Mise en place d'un registre privé d'images Docker

## Préparation du système

Déclaration de variables de configuration contextuelle&nbsp;:

```bash
export HARBOR_VERSION=$(curl --head --silent --url 'https://github.com/vmware/harbor/releases/latest' | grep -oP '^(?:Location).+$' | awk -F'/' '{print $(NF)}' | grep -oP 'v([0-9]\.?){2,}(-[a-zA-Z0-9]+)?')
export HARBOR_HOSTNAME=$(hostname -f)
export HARBOR_LISTENING=https
export HARBOR_DB_PASSWORD=LUFQwV1dIqoqrX420KVc5
export HARBOR_ADMIN_PASSWORD=TQwoqOpcd6oXsmg2VoQB
export HARBOR_CERT=/etc/ssl/certs/$(hostname -s).crt
export HARBOR_KEY=/etc/ssl/private/$(hostname -s).key
export HARBOR_SECRETKEY_PATH=/data/
```

### Création du certification

Création d'une clé privée pour le serveur de registre et génération du CSR (depuis le serveur `registry`)&nbsp;:

```bash
openssl req \
  -newkey rsa:4096 -nodes -sha256 -keyout /etc/ssl/private/registry.key \
  -out registry.csr
cat <<-EOF >additional.cnf
subjectAltName = @alt_names

[alt_names]
DNS.1 = $(hostname -f)
DNS.2 = $(hostname -s)
IP = $(ip addr show eth0 | grep -oP "inet[ ]+[^ ]+" | awk -F' ' '{print $2}')

EOF
```

Création d'une CA sur le poste client ou sur un serveur dédié (facultatif si existe déjà)&nbsp;:

```bash
openssl req \
  -newkey rsa:4096 -nodes -sha256 -keyout ca.key \
  -x509 -days 365 -out ca.crt
```

Récupération et signature du certificat du serveur `registry`&nbsp;:

```bash
scp registry:registry.csr ./
openssl x509 \
  -req -days 365 -in registry.csr \
  -CA ca.crt -CAkey ca.key -CAcreateserial -extfile additional.cnf \
  -out registry.crt
```

Envoi du certificat signé sur le serveur `registry` et sur les clients (noeuds du Docker Swarm)&nbsp;:

```bash
scp registry.crt registry:/etc/ssl/certs/
for HOST in manager1 manager2 node1 node2 dns registry; do
    scp ca.crt ${HOST}:/usr/local/share/ca-certificates/ca.crt
    ssh ${HOST} 'install -D /{usr/local/share/ca-certificates,etc/docker/certs.d/registry.docker.lab}/ca.crt && update-ca-certificates --fresh'
done
```

## Installation de Harbor

Téléchargement de Harbor&nbsp;:

```bash
cd /opt/
wget https://github.com/vmware/harbor/releases/download/${HARBOR_VERSION}/harbor-offline-installer-${HARBOR_VERSION}.tgz
tar xvzf harbor-offline-installer-${HARBOR_VERSION}.tgz
pushd harbor/
```

Configuration de Harbor&nbsp;:

```bash
cp harbor.cfg{,.bak}
perl -p -i -e '
    s@(hostname[\s\t]*=[\s\t]*).+@\1'${HARBOR_HOSTNAME}'@;
    s@(ui_url_protocol[\s\t]*=[\s\t]*).+@\1'${HARBOR_LISTENING}'@;
    s@(db_password[\s\t]*=[\s\t]*).+@\1'${HARBOR_DB_PASSWORD}'@;
    s@(harbor_admin_password[\s\t]*=[\s\t]*).+@\1'${HARBOR_ADMIN_PASSWORD}'@;
    s@(customize_crt[\s\t]*=[\s\t]*).+@\1off@;
    s@(ssl_cert[\s\t]*=[\s\t]*).+@\1'${HARBOR_CERT}'@;
    s@(ssl_cert_key[\s\t]*=[\s\t]*).+@\1'${HARBOR_KEY}'@;
    s@(secretkey_path[\s\t]*=[\s\t]*).+@\1'${HARBOR_SECRETKEY_PATH}'@
' harbor.cfg
```

Installation de Harbor&nbsp;:

```bash
./prepare
docker-compose down --remove-orphans && ./install.sh && docker-compose up -d

# Pour la fonction notary :
# docker-compose -f ./docker-compose.yml -f ./docker-compose.notary.yml down --remove-orphans && ./install.sh && docker-compose -f ./docker-compose.yml -f ./docker-compose.notary.yml up -d
```

Affichage des journaux d'événements&nbsp;:

```bash
tail -f /var/log/harbor/$(date +%Y-%m-%d)/*.log
```
