# DNS

## Installation du DNS

```bash
apt-get -fy install bind9
```

## Configuration du DNS

Ajout d'une zone de recherche directe et une zone inverse à la base de configuration de named&nbsp;:

```bash
cat <<-EOF >/etc/bind/named.conf.local
zone "${DOMAIN}" {
    type master;
    file "/etc/bind/zones/db.${DOMAIN}";
};

zone "57.16.172.in-addr.arpa" {
    type master;
    file "/etc/bind/zones/db.${DOMAIN}.rev";
};

EOF
install -m 2775 -o root -g bind -d /etc/bind/zones/
```

Création d'une zone de recherche directe&nbsp;:

```bash
cat <<-EOF >/etc/bind/zones/db.${DOMAIN}
\$ORIGIN            .        ; Origin from ROOT
\$TTL               3600     ; Time-to-live (1 hour)
${DOMAIN}           SOA      dns.${DOMAIN}. admin.${DOMAIN}. (
                            1               ; Serial
                            10800           ; Refresh (3 hours)
                            3600            ; Retry (1 hour)
                            604800          ; Expire (1 week)
                            300 )           ; Minimum TTL (5 minutes)

                   NS         dns.${DOMAIN}.     ; Domain server

\$ORIGIN            ${DOMAIN}.
;
; Hosts
;
manager1           A        172.16.57.224
manager2           A        172.16.57.225
node1              A        172.16.57.226
node2              A        172.16.57.227
registry           A        172.16.57.228
dns                A        172.16.57.229

;
; Multi-homed hosts
;
manager            A        172.16.57.224
manager            A        172.16.57.225
worker             A        172.16.57.226
worker             A        172.16.57.227

EOF
```

Création d'une zone inverse&nbsp;:

```bash
cat <<-EOF >/etc/bind/zones/db.${DOMAIN}.rev
\$ORIGIN            .        ; Origin from ROOT
\$TTL               3600     ; Time-to-live (1 hour)
57.16.172.in-addr.arpa           SOA       ddns.${DOMAIN}. admin.${DOMAIN}. (
                                      1               ; Serial
                                      10800           ; Refresh (3 hours)
                                      3600            ; Retry (1 hour)
                                      604800          ; Expire (1 week)
                                      300 )           ; Minimum TTL (5 minutes)

                   NS         dns.${DOMAIN}.     ; Domain server

\$ORIGIN           57.16.172.in-addr.arpa.
;
; Hosts
;
224                PTR      manager1.${DOMAIN}.
225                PTR      manager2.${DOMAIN}.
226                PTR      node1.${DOMAIN}.
227                PTR      node2.${DOMAIN}.
228                PTR      registry.${DOMAIN}.
229                PTR      dns.${DOMAIN}.

EOF
```

Vérification récursive des configurations&nbsp;:

```bash
named-checkconf -z /etc/bind/named.conf
```

Configuration du service DNS&nbsp;:

```bash
cat <<-'EOF' >/etc/default/bind9
# run resolvconf?
RESOLVCONF=no

# startup options for the server
OPTIONS="-4 -u bind"

EOF

cat <<-EOF >/etc/bind/named.conf.options
options {
    directory "/var/cache/bind";
    auth-nxdomain no;  # Conform to RFC1035
    clients-per-query 20;
    max-clients-per-query 30;
    cleaning-interval 15;

    dnssec-validation no;
//     dnssec-validation yes;
//     dnssec-enable yes;
//     dnssec-lookaside auto;

    listen-on port 53 {
        any;
    };

    listen-on-v6 port 53 {
        none;
    };

    recursion yes;
    allow-recursion {  # IP allowed to send recursive DNS queries to the server
        172.16.0.0/16;
        127.0.0.0/8;
    };

    forward first;
    forwarders {
        ${DNS2} port 53;
        ${DNS3} port 53;
    };

    allow-query {  # IP allowed to send DNS queries to the server
        172.16.0.0/16;
        127.0.0.0/8;
    };

    allow-transfer {  # IP allowed to get copy of DNS Zone data
        10.1.2.0/24;
        127.0.0.0/8;
    };
};

EOF
```

Redémarrage et test du service DNS&nbsp;:

```bash
systemctl restart bind9.service
dig ${DOMAIN} ANY +noall +answer +nocomments
```
