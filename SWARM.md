# Mise en place d'un dispositif d'orchestration de services avec Docker Swarm

## Création du réseau

Depuis `master1`, création d'un noeud avec le rôle master pour initier un Docker Swarm&nbsp;:

```bash
docker swarm init --advertise-addr ${IP} --listen-addr ${IP}
```

Affichage des jetons (tokens) pour rejoindre le Docker Swarm avec le rôle worker ou master&nbsp;:

```bash
docker swarm join-token worker
docker swarm join-token manager
```

Connexion des noeuds au Docker Swarm tel que&nbsp;:

| Noeud                 | Rôle   |
|-----------------------|--------|
| master1 (déjà ajouté) | master |
| master2               | master |
| worker1               | worker |
| worker2               | worker |

Liste des noeuds composants le swarm&nbsp;:

```bash
docker node ls
```

## Déploiement d'un micro-service

### Déploiement avec Docker-Engine

```bash
docker service create \
  --name hello-world \
  --publish 80:80/tcp \
  --replicas 3 \
  tutum/hello-world
```

Affichage du status du service&nbsp;:

```bash
docker service ps hello-world
```

Extension du service&nbsp;:

```bash
docker service scale hello-world=4
```

Vérification de fonctionnement du service&nbsp;:

```bash
for HOST in $(docker service ps hello-world --filter "desired-state=running" | tail -n+2 | awk '{print $4}'); do printf "[+] \033[1;32m%s\033[0m\n\n%s\n" "${HOST}" "$(curl --silent --location --include --head --url "http://${HOST}/")"; done
```

Suppression du service&nbsp;:

```bash
docker service rm hello-world
```

### Déploiement avec Docker-Compose

```bash
cd /opt/
git clone https://gitlab.com/Creased/docker-swarm-hello-world
pushd docker-swarm-hello-world/
docker login --username Creased registry.docker.lab
docker-compose build
docker-compose push
docker stack deploy --with-registry-auth --compose-file docker-compose.yml hello-world
```

Affichage du status des services&nbsp;:

```bash
docker service ls
docker service ps --no-trunc hello-world_http
docker service ps --no-trunc hello-world_redis
```

Vérification de fonctionnement du service&nbsp;:

```bash
export SWARM_SERVICE=hello-world_http
for SWARM_HOST in $(docker node ls --filter "membership=accepted" | sed -r -e 's@^([^ ]+ )\*@\1@' | tail -n+2 | awk '{print $2}' | sort); do printf "[+] \033[1;32m%s\033[0m\n\n%s\n\n" "${SWARM_HOST}" "$(curl --silent --location --include --url "http://${SWARM_HOST}/")"; done
```

Suppression du service&nbsp;:

```bash
docker stack rm hello-world
```
