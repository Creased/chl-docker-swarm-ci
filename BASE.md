# Configuration de base de Linux Debian Jessie

## Préparation du système

Déclaration de variables de configuration contextuelle **sur le serveur**&nbsp;:

```bash
export HOSTNAME=manager1
export IP=172.16.57.224
```

```bash
export HOSTNAME=manager2
export IP=172.16.57.225
```

```bash
export HOSTNAME=node1
export IP=172.16.57.226
```

```bash
export HOSTNAME=node2
export IP=172.16.57.227
```

```bash
export HOSTNAME=registry
export IP=172.16.57.228
```

```bash
export HOSTNAME=dns
export IP=172.16.57.229
```

```bash
export DOMAIN=docker.lab
export NIC=eth0
export MASK=255.255.0.0
export GW=172.16.10.1
export DNS1=172.16.57.229
export DNS2=172.16.10.32
export DNS3=172.16.10.33
export PROXY=172.18.4.1:8080
export NOPROXY=localhost,127.0.0.0/8,.miletrie.chl,.docker.lab
export USR=user
export GRP=users
export USRID=1337
export LANG=fr_FR.UTF-8
export LOCALE=Europe/Paris
```

Configuration de la langue&nbsp;:

```bash
cat <<-'EOF' >/etc/timezone
${LOCALE}
EOF
sed -i -e 's/# \('${LANG}'\)/\1/' /etc/locale.gen
locale-gen ${LANG}
dpkg-reconfigure -f noninteractive tzdata
```

### Configuration de l'accès réseau

Configuration des contrôleurs d'interfaces réseaux et de la résolution de noms DNS&nbsp;:

```bash
cat <<-EOF >/etc/network/interfaces
##
# Default configuration
#
iface default inet dhcp

##
# Loopback NIC configuration
#
auto lo
iface lo inet loopback

##
# Ethernet NIC configuration
#
auto ${NIC}
allow-hotplug ${NIC}
iface ${NIC} inet static
    address ${IP}
    netmask ${MASK}
    gateway ${GW}
    dns-search ${DOMAIN}
    dns-nameservers ${DNS1} ${DNS2} ${DNS3}

EOF
```

Application des configurations au contrôleur d'interface réseau Ethernet&nbsp;:

```bash
ifdown eth0
ifup eth0
```

Régénération du fichier /etc/resolv.conf&nbsp;:

```bash
resolvconf -u || systemctl restart networking.service || /etc/init.d/networking restart
cat <<-EOF >/etc/resolv.conf
domain ${DOMAIN}
search ${DOMAIN}

nameserver ${DNS1}
nameserver ${DNS2}
nameserver ${DNS3}

EOF
```

Vérification de la résolution de nom et de l'accès réseau&nbsp;:

```bash
dig ${DOMAIN} ANY +noall +answer +nocomments
```

Configuration du proxy&nbsp;:

```bash
cat <<-EOF >>~/.bash_profile
HTTP_PROXY="http://${PROXY}/"
HTTPS_PROXY="\${HTTP_PROXY}"
NO_PROXY="${NOPROXY}"
http_proxy="\${HTTP_PROXY}"
https_proxy="\${HTTPS_PROXY}"
no_proxy="\${NO_PROXY}"

export HTTP_PROXY HTTPS_PROXY NO_PROXY http_proxy https_proxy no_proxy

EOF
source ~/.bash_profile
printenv HTTP_PROXY HTTPS_PROXY NO_PROXY http_proxy https_proxy no_proxy
```

### Configuration du nom d'hôte

Mise à jour du nom d'hôte associé au système&nbsp;:

```bash
cat <<-EOF >/etc/hostname
${HOSTNAME}

EOF
cat <<-EOF >/etc/hosts
# IPv4
127.0.0.1 localhost
127.0.1.1 ${HOSTNAME}.${DOMAIN} ${HOSTNAME}

# IPv6
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts

EOF
hostname ${HOSTNAME}.${DOMAIN}
hostname -b ${HOSTNAME}.${DOMAIN}
domainname ${HOSTNAME}.${DOMAIN}
domainname -b ${HOSTNAME}.${DOMAIN}
hostnamectl set-hostname ${HOSTNAME}.${DOMAIN}
sysctl kernel.hostname=${HOSTNAME}.${DOMAIN}
systemctl restart networking.service || /etc/init.d/networking restart
```

### Configuration du gestionnaire de paquets APT

Configuration du proxy pour le gestionnaire de paquets&nbsp;:

```bash
cat <<-EOF >>/etc/apt/apt.conf.d/70debconf
Acquire::http::Proxy "http://${PROXY}/";
Acquire::https::Proxy "https://${PROXY}/";
Acquire::socks::Proxy "socks://${PROXY}/";

EOF
```

Mise à jour des dépôts puis mise à niveau des paquets et de la release système&nbsp;:

```bash
cat <<-'EOF' >/etc/apt/sources.list
# Dépôt de base de Debian Jessie
deb http://httpredir.debian.org/debian/ jessie main contrib
deb-src http://httpredir.debian.org/debian/ jessie main contrib

# Mises à jour distribution stable
deb http://httpredir.debian.org/debian/ jessie-updates main
deb-src http://httpredir.debian.org/debian/ jessie-updates main

# Mises à jour vers distribution stable
deb http://httpredir.debian.org/debian/ jessie-backports main
deb-src http://httpredir.debian.org/debian/ jessie-backports main

# Mises à jour de sécurité
deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main

EOF
```

Téléchargement et installation de la clé GPG *(&laquo;&nbsp;GNU Privacy Guard&nbsp;&raquo;)* du dépôt de Debian&nbsp;:

```bash
gpg --keyserver pgpkeys.mit.edu --recv-key 8B48AD6246925553
gpg -a --export 8B48AD6246925553 | apt-key add -
```

Mise à niveau des paquets et du noyau système&nbsp;:

```bash
apt-get update
apt-get -fy upgrade
apt-get -fy dist-upgrade
```

### Configuration des utilisateurs

Création d'un utilisateur simple&nbsp;:

```bash
useradd -u ${USRID} -UG cdrom,floppy,audio,dip,video,plugdev,netdev,ssh-users,${GRP} -s /bin/bash -d /home/${USR}/ ${USR}
mkdir /home/${USR}/
find /etc/skel/ -mindepth 1 -exec cp -r {} /home/${USR}/ \;
chown -R ${USR}:${USR} /home/${USR}/
chmod -R 700 /home/${USR}/
passwd ${USR}
```

Configuration de bash&nbsp;:

```bash
cat <<-'EOF' >/etc/skel/.bashrc
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w \$\[\033[00m\] '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

EOF
install -m 644 -o ${USR} -g ${GRP} /{etc/skel,home/${USR}}/.bashrc
install -m 644 -o root -g root /{etc/skel,root}/.bashrc
sed -i 's#\\\[\\033\[01;32m\\\]#\\\[\\033\[01;31m\\\]#' /root/.bashrc
echo "source /root/.bashrc" >>/root/.bash_profile
```

Suppression de la gestion sudo, l'exécution de commandes d'administrations sensibles ne seront possibles que dans un contexte d'exécution privilégié (root) non hérité&nbsp;:

```bash
apt-get purge sudo
```

**OU** Ajout de l'utilisateur aux membres du groupe sudo pour l'exécution rapide de tâches dans un contexte d'exécution privilégié&nbsp;:

```bash
apt-get install sudo
usermod -aG sudo ${USR}
```

# Préparation de l'espace de travail (Workbench)

Installation d'utilitaires usuels&nbsp;:

```bash
apt-get -fy install curl dnsutils git mercurial netcat ntpdate python3.4 resolvconf subversion vim build-essential apt-utils
```

Configuration de VIM&nbsp;:

```bash
cat <<-'EOF' >/etc/skel/.vimrc
set number

" Fast saving
nmap <leader>w :w!<cr>

" Enable syntax highlighting
syntax enable

" Turn on the WiLd menu
set wildmenu

"Always show current position
set ruler

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Show matching brackets when text indicator is over them
set showmatch

" How many tenths of a second to blink when matching brackets
set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

colorscheme desert
set background=dark

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
map <space> /
map <c-space> ?

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
          \   exe "normal! g`\"" |
           \ endif

" Remember info about open buffers on close
set viminfo^=%

EOF
chmod 644 /etc/skel/.vimrc
install -m 700 -o ${USR} -g ${USR} /etc/skel/.vimrc /home/${USR}/
install -m 700 -o root -g root /etc/skel/.vimrc /root/
```

Configuration de la date et heure en utilisant un service NTP *(&laquo;&nbsp;Network Time Protocol&nbsp;&raquo;)*&nbsp;:

```bash
ntpdate 0.fr.pool.ntp.org
```

## Installation de OpenSSH-Server

Téléchargement du paquet openssh-server en utilisant le gestionnaire APT&nbsp;:

```bash
apt-get -fy install openssh-server openssh-sftp-server openssh-client
```

Arrêt du service SSH pour préparer sa configuration&nbsp;:

```bash
/etc/init.d/ssh stop || systemctl stop ssh.service
```

Génération de nouveaux nombres premiers de 4096 bits pour le Diffie-Hellman Group Exchange (à défaut d'unité de calcul graphique, cette étape est très coûteuse en termes de temps processeur)&nbsp;:

```bash
ssh-keygen -G /tmp/moduli -b 4096
```

Test de sûreté et validation des nombres premiers et suppression des nombres premiers&nbsp;:

```bash
ssh-keygen -T /etc/ssh/moduli -f /tmp/moduli
rm /tmp/moduli
```

Génération d'un nouveau jeu de clé publique et privée pour le serveur&nbsp;:

```bash
rm /etc/ssh/ssh_host_*key*
ssh-keygen -N '' -t ed25519 -f /etc/ssh/ssh_host_ed25519_key </dev/null
ssh-keygen -N '' -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key </dev/null
```

Ajout d'un message d'avertissement pour éviter tout problème en cas de procédures judiciaires suite à un potentiel piratage ou autre accès non autorisé&nbsp;:

```bash
cat >/etc/issue <<-EOF
#############################################
#  AVERTISSEMENT: L'accès à ce système est  #
#  restreint au personnel administratif de  #
#  la direction des systèmes d'information  #
#    du Centre Hospitalier Henri Laborit    #
#                                           #
#   Tout accès non autorisé constitue une   #
# violation sujette à poursuite judiciaire. #
#############################################

EOF
```

### Configuration du client

Sauvegarde de la configuration d'origine&nbsp;:

```bash
cp /etc/ssh/ssh_config{,.bak}
```

Création d'une nouvelle configuration pour l'authentification SSH en tant que client&nbsp;:

```bash
cat <<-'EOF' >/etc/ssh/ssh_config
# Configuration par défaut pour l'authentification sur tous les hôtes
Host *
    # Pas d'authentification par mot de passe
    PasswordAuthentication no
    ChallengeResponseAuthentication no

    # Authentification par clés publiques
    PubkeyAuthentication yes

    # Privilégie l'utilisation de EdDSA (Ed25519) à RSA
    IdentityFile ~/.ssh/id_ed25519
    IdentityFile ~/.ssh/id_rsa

    # Désactive le Roaming (CVE-2016-0777 et CVE-2016-0778)
    UseRoaming no

    # Variables d'environnement relatives à la langue
    SendEnv LANG LC_*

    # N'affiche pas les noms d'hôtes en clair
    HashKnownHosts yes

    # Divers
    VisualHostKey yes

EOF
```

### Configuration du service

Sauvegarde de la configuration d'origine&nbsp;:

```bash
cp /etc/ssh/sshd_config{,.bak}
```

Création d'une nouvelle configuration pour le service SSH&nbsp;:

```bash
cat <<-'EOF' >/etc/ssh/sshd_config
#### Réseau ####

# Port d'écoute du service, non standard conseillé afin d'éviter les scans intempestifs (n'augmente en aucun cas le niveau de sécurité !)
Port 22

# Accélération du processus d'authentification pour les clients n'utilisant pas de paramètres DNS valides
UseDNS no

# Utilisation du protocole SSH-2 uniquement
Protocol 2

# Écoute IPv4 uniquement et sur le réseau d'administration
AddressFamily inet
ListenAddress 0.0.0.0

# Désactivation de la redirection du flux Xorg
X11Forwarding no
X11DisplayOffset 10

# Désactive les messages de type TCPKeepAlive, ces messages ne sont pas diffusés dans le canal chiffré
TCPKeepAlive no

# Alternative chiffrée au TCPKeepAlive, désauthentification après 3 min d'inactivité (3*60sec)
ClientAliveInterval 60
ClientAliveCountMax 3

#### Clés ####

# Clés d'hôte pour le protocole SSH-2
# Privilégie l'utilisation de EdDSA (Ed25519) à RSA
HostKey /etc/ssh/ssh_host_ed25519_key
HostKey /etc/ssh/ssh_host_rsa_key

# Séparation des privilèges, prévient les exploits de type privilege escalation
UsePrivilegeSeparation yes
UseLogin no

# Authentification par clé publique
PubkeyAuthentication yes
AuthorizedKeysFile %h/.ssh/authorized_keys

#### Authentification ####

# Whitelist
AllowGroups ssh-users

# 30 secondes pour saisir la passphrase
LoginGraceTime 30

# Interdit l'authentification avec un utilisateur root
PermitRootLogin no

# Vérification des permissions sur les fichiers de clés et les répertoires
StrictModes yes

# N'utilise pas les fichiers ~/.rhosts et ~/.shosts
IgnoreRhosts yes

# Pas d'authentification basée sur l'hôte
HostbasedAuthentication no

# Ne fait pas confiance en ~/.ssh/known_hosts pour l'authentification RhostsRSAAuthentication
IgnoreUserKnownHosts yes
RSAAuthentication yes
RhostsRSAAuthentication no

# Interdit l'authentification sans mot de passe
PermitEmptyPasswords no

# Utilisation de clés publique/privée pour l'authentification
PasswordAuthentication no

# Désactivation du ChallengeResponse (à activer pour une authentification TOTP)
UsePAM no
ChallengeResponseAuthentication no

#### Divers ####

# Journalisation dans /var/log/auth.log
SyslogFacility AUTH
LogLevel INFO

# Affichage de la dernière connexion
PrintLastLog yes

MaxAuthTries 2
MaxStartups 10:30:60

# Affichage d'une bannière de login
Banner /etc/issue
PrintMotd no

# Autorise les clients à passer des variables locales d'environnement
AcceptEnv LANG LC_*

Subsystem sftp /usr/lib/openssh/sftp-server

EOF
```

## Configuration de l'utilisateur &laquo;&nbsp;user&nbsp;&raquo;

Création d'un groupe et ajout de l'utilisateur autorisé pour l'authentification sur le service SSH (voir Whitelist dans [Installation de OpenSSH-Server](#installation-de-openssh-server))&nbsp;:

```bash
groupadd ssh-users
usermod -aG ssh-users ${USR}
```

Création des jeux de clés EdDSA (Edwards-curve Digital Signature Algorithm) et RSA (Rivest-Shamir-Addleman) pour l'authentification par clé privée (**depuis le serveur**)&nbsp;:

```bash
mkdir /home/${USR}/.ssh/
ssh-keygen -t ed25519 -f /home/${USR}/.ssh/${HOSTNAME}_ed25519
ssh-keygen -t rsa -b 4096 -f /home/${USR}/.ssh/${HOSTNAME}_rsa
```

**<u>Facultatif&nbsp;:</u>** Création d'un jeu de clés EdDSA et RSA résistant aux attaques de type bruteforce, cette étape est recommandée, mais dans la mesure où elle contraint à l'utilisation d'un client SSH supportant le processus cryptographique de dérivation de clé (*&laquo;&nbsp;key derivation function&nbsp;&raquo;* ou KDF), il sera nécessaire d'apprécier les enjeux relatifs à l'utilisation d'un tel jeu de clé, notamment en termes de portabilité et rétrocompatibilité&nbsp;:

```bash
ssh-keygen -t ed25519 -o -a 100 -f /home/${USR}/.ssh/${HOSTNAME}_ed25519
ssh-keygen -t rsa -b 4096 -o -a 100 -f /home/${USR}/.ssh/${HOSTNAME}_rsa
```

Autorisation des clés publiques (**depuis le serveur**)&nbsp;:

```bash
cat /home/${USR}/.ssh/${HOSTNAME}_{ed25519,rsa}.pub >/home/${USR}/.ssh/authorized_keys
```

Affichage des clés privées et des paramètres du **client SSH** (**depuis le serveur**)&nbsp;:

```bash
cat <<-_EOF_
# Commandes à lancer sur le client
cat <<-'EOF' >~/.ssh/${HOSTNAME}_ed25519
$(cat /home/${USR}/.ssh/${HOSTNAME}_ed25519)
EOF
cat <<-'EOF' >~/.ssh/${HOSTNAME}_rsa
$(cat /home/${USR}/.ssh/${HOSTNAME}_rsa)
EOF
cat <<-'EOF' >>~/.ssh/config
Host ${HOSTNAME}
  Hostname ${IP}
  User ${USR}
  IdentityFile ~/.ssh/${HOSTNAME}_ed25519
  IdentityFile ~/.ssh/${HOSTNAME}_rsa

EOF
touch ~/.ssh/known_hosts
ssh ${HOSTNAME}
_EOF_
```

Mise à jour des droits d'accès aux configurations SSH de l'utilisateur &laquo;&nbsp;user&nbsp;&raquo; et suppression des clés (**depuis le serveur**)&nbsp;:

```bash
chown -R ${USR}:${USR} /home/${USR}/.ssh/
chmod 0500 /home/${USR}/.ssh/
chmod 0400 /home/${USR}/.ssh/authorized_keys
rm /home/${USR}/.ssh/${HOSTNAME}_{ed25519,rsa}{,.pub}
```

Démarrage du service SSH&nbsp;:

```bash
/etc/init.d/ssh start || systemctl start ssh.service
```
