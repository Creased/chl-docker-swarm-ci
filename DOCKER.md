# Installation et configuration de Docker-CE sur Debian Jessie (sur tous les noeuds)

## Suppression des anciennes versions

Depuis la [release 17.03](https://github.com/docker/docker.github.io/pull/2050), le nom du package contenant le moteur Docker-Engine s'appelle `docker-ce`, avant de procéder à son installation, il faut donc procéder à la suppression des anciens paquets&nbsp;:

```bash
apt-get remove docker docker-engine
```

<u>Note :</u> Le contenu de `/var/lib/docker/`, comprenant les images, conteneurs, volumes et réseaux est préservé lors de la suppression du paquet.

## Configuration des dépôts APT

Pour installer Docker en utilisant le gestionnaire de paquets APT, il est nécessaire de configurer un nouveau dépôt dans les sources de APT. Cependant, afin d'exploiter ce nouveau dépôt sur HTTPS, il est nécessaire d'installer quelques outils&nbsp;:

```bash
apt-get -fy install curl ca-certificates apt-transport-https software-properties-common
```

Téléchargement de la clé GPG de Docker&nbsp;:

```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
```

Ajout dépôt&nbsp;:

```bash
cat <<-'EOF' >/etc/apt/sources.list.d/docker.list
deb [arch=amd64] https://download.docker.com/linux/debian jessie stable

EOF
```

Mise à niveau des caches&nbsp;:

```bash
apt-get clean
apt-get update
```

## Installation de Docker-CE

```bash
apt-get -fy install docker-ce
```

# Configuration de Docker-CE sur Debian Jessie

## Création d'un utilisateur de service

Par défaut, le daemon docker est en écoute sur un socket UNIX, dont le propriétaire et la gestion dépend de l'utilisateur root, par conséquent, seules les utilisateurs disposant d'un accès à un shell intéractif dans le contexte d'exécution de root peuvent y accèder.

Afin de passer outre cette limitation, le daemon docker supporte l'utilisation d'un groupe UNIX appelé `docker`.

Création du groupe `docker`&nbsp;:

```bash
groupadd docker
```

Ajout de l’utilisateur aux membres du groupe docker&nbsp;:

```bash
usermod -aG docker ${USR}
```

## Configuration de Docker pour démarrer automatiquement

```bash
systemctl enable docker
```

## Configuration du service

```bash
cat <<-'EOF' >/etc/default/docker
##
# Location of Docker binary (especially for development testing)
#
DOCKERD="/usr/bin/dockerd"

##
# General daemon startup options
# https://docs.docker.com/engine/reference/commandline/docker/
#
DOCKER_OPTS="--ip=0.0.0.0 \
             --ip-masq=false \
             --ip-forward=false \
             --iptables=false \
             --userland-proxy=false \
             --icc=true"

EOF
mkdir /etc/systemd/system/docker.service.d/
cat <<-'EOF' >/etc/systemd/system/docker.service.d/exec.conf
[Service]
EnvironmentFile=-/etc/default/docker
ExecStart=
ExecStart=/usr/bin/dockerd $DOCKER_OPTS

EOF
systemctl daemon-reload
systemctl restart docker.service docker.socket
```

Configuration du proxy&nbsp;:

```bash
cat <<-EOF >/etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=${HTTP_PROXY}"
Environment="HTTPS_PROXY=${HTTPS_PROXY}"
Environment="NO_PROXY=${NO_PROXY}"
Environment="http_proxy=${http_proxy}"
Environment="https_proxy=${https_proxy}"
Environment="no_proxy=${no_proxy}"

EOF
systemctl daemon-reload
systemctl restart docker.service docker.socket
```

Ajout de fonctions pour la configuration de DockerD&nbsp;:

```bash
cat <<-'EOF' >>~/.bash_profile
cdr2mask () {
    ## https://forums.gentoo.org/viewtopic-t-888736-start-0.html
    # Number of args to shift, 255..255, first non-255 byte, zeroes
    set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
    [ $1 -gt 1 ] && shift $1 || shift
    echo ${1-0}.${2-0}.${3-0}.${4-0}
}

#======================================#
# [+] Title:   DockerD Options Editor  #
# [+] Author:  Baptiste M. (Creased)   #
# [+] Website: bmoine.fr               #
# [+] Email:   contact@bmoine.fr       #
# [+] Twitter: @Creased_               #
#======================================#

dockerd-del-opt() {
    ###
    # Variables
    #
    OPTIONS=${1:-''}
    FILE=/etc/default/docker

    ###
    # Main process
    #
    if [ ! -z "${OPTIONS}" ]; then
        for OPT in $OPTIONS; do (
            # Display the ongoing operation
            printf "\033[0;33m[-]\033[0m \033[1;36m%s\033[0m\n" "${OPT}"

            # Escape option in order to prevent sed to treat special chars
            OPT=$(echo "${OPT}" | sed -e 's/[]\/$*.^|[]/\\&/g')

            sed -i -e '/DOCKER_OPTS="/,/"/{
                :a;N;$!ba;                                              # Add all lines to buffer
                s/^\(DOCKER_OPTS="\)\([^\n\r]*\)/\1\n             \2/;  # Handle empty DOCKER_OPTS by adding a new temporary line
                :remove;{                                               # Remove option within DOCKER_OPTS
                    s/\([ \\]\)[\r\n]*[^\r\n]*--'${OPT}'\(\(=[^="\r\n]*\)\|\([^=a-zA-Z0-9"\r\n]*\)\)\(["\r\n]\)/\1\5/;
                    s/ \\\("\n$\)/\1/
                }; t remove;
                s/^\(DOCKER_OPTS="\)[\n ]*\([^\n\r]*\)/\1\2/            # Remove temporary line
            }' ${FILE}
        ); done
        printf "\n"
    fi

    ###
    # Display changes
    #
    cat ${FILE}
}

dockerd-add-opt() {
    ###
    # Variables
    #
    OPTIONS=${1:-''}
    FILE=/etc/default/docker

    ###
    # Main process
    #
    if [ ! -z "${OPTIONS}" ]; then
        # Prevent duplicate by removing existant option
        dockerd-del-opt "${OPTIONS}"

        for OPT in $OPTIONS; do (
            # Display the ongoing operation
            printf "\033[0;33m[+]\033[0m \033[1;36m%s\033[0m\n" "${OPT}"

            # Escape option in order to prevent sed to treat special chars
            OPT=$(echo "${OPT}" | sed -e 's/[]\/$*.^|[]/\\&/g')

            sed -i -e '/DOCKER_OPTS="/,/"/{
                :a;N;$!ba;                                                        # Add all lines to buffer
                s/^\(DOCKER_OPTS="[^"][^"]*\)"/\1 \\\n             --'${OPT}'"/;  # Add option within DOCKER_OPTS
                s/^\(DOCKER_OPTS="\)"/\1--'${OPT}'"/                              # Add option within empty DOCKER_OPTS
            }' ${FILE}
        ); done
        printf "\n"
    fi

    ###
    # Display changes
    #
    cat ${FILE}
}

EOF
source ~/.bash_profile
```

# Configuration de Docker-CE

## Définition des variables contextuelles

```bash
export BR_FRONT=br0
export IP_FRONT_SUBNET=10.0.0.0/24
export IP_FRONT_RANGE=10.0.0.0/24
export IP_FRONT_CIDR=10.0.0.254/24
export IP_FRONT=$(printf "%s" "${IP_FRONT_CIDR}" | awk -F'/' '{print $1}')
export IP_FRONT_MASK=$(cdr2mask $(printf "%s" "${IP_FRONT_CIDR}" | awk -F'/' '{print $2}'))

export BR_BACK=br1
export IP_BACK_SUBNET=10.1.0.0/24
export IP_BACK_RANGE=10.1.0.0/24
export IP_BACK_CIDR=10.1.0.254/24
export IP_BACK=$(printf "%s" "${IP_BACK_CIDR}" | awk -F'/' '{print $1}')
export IP_BACK_MASK=$(cdr2mask $(printf "%s" "${IP_BACK_CIDR}" | awk -F'/' '{print $2}'))

export IP_GWB_SUBNET=10.2.0.0/24
export IP_GWB_RANGE=10.2.0.0/24
export IP_GWB_CIDR=10.2.0.254/24
export IP_GWB=$(printf "%s" "${IP_GWB_CIDR}" | awk -F'/' '{print $1}')
export IP_GWB_MASK=$(cdr2mask $(printf "%s" "${IP_GWB_CIDR}" | awk -F'/' '{print $2}'))
```

Debug&nbsp;:

```bash
VAR="NIC IP MASK GW DNS1 DNS2 BR_FRONT IP_FRONT_SUBNET IP_FRONT_RANGE IP_FRONT_CIDR IP_FRONT IP_FRONT_MASK BR_BACK IP_BACK_SUBNET IP_BACK_RANGE IP_BACK_CIDR IP_BACK IP_BACK_MASK IP_GWB_SUBNET IP_GWB_RANGE IP_GWB_CIDR IP_GWB IP_GWB_MASK"
for V in ${VAR}; do (
    printf "%-20s => %s\n" "${V}" "${!V}"
); done
```

## Arrêt des services Docker

```bash
systemctl stop docker.service docker.socket
```

## Installation des outils de configuration de ponts

```bash
apt-get -fy install bridge-utils
```

## Suppression du pont par défaut

```bash
ip link set dev docker0 down
brctl delbr docker0
```

## Configuration des ponts

```bash
cat <<-EOF >>/etc/network/interfaces
##
# Bridges configuration
#
auto ${BR_FRONT}
iface ${BR_FRONT} inet static
    bridge_ports none
    address ${IP_FRONT}
    netmask ${IP_FRONT_MASK}

auto ${BR_BACK}
iface ${BR_BACK} inet static
    bridge_ports none
    address ${IP_BACK}
    netmask ${IP_BACK_MASK}

EOF
ifdown ${BR_FRONT} && ifup ${BR_FRONT}
ifdown ${BR_BACK} && ifup ${BR_BACK}
```

Création d'un nouveau réseau de Frontend dans Docker&nbsp;:

```bash
dockerd-del-opt "bip fixed-cidr dns bridge ip ip-forward userland-proxy ip-masq iptables icc"
dockerd-add-opt "ip=${IP} ip-forward=true userland-proxy=true ip-masq=true iptables=true icc=true bridge=${BR_FRONT}"
systemctl daemon-reload
systemctl restart docker.service docker.socket
```

Mise à jour du réseau de passerelles Docker&nbsp;:

```bash
docker network rm docker_gwbridge
docker network create \
       --driver bridge \
       --subnet=${IP_GWB_SUBNET} \
       --ip-range=${IP_GWB_RANGE} \
       --gateway=${IP_GWB} \
       --internal=false \
       --opt "com.docker.network.bridge.default_bridge"="false" \
       --opt "com.docker.network.bridge.enable_icc"="false" \
       --opt "com.docker.network.bridge.enable_ip_masquerade"="true" \
       --opt "com.docker.network.bridge.name"="docker_gwbridge" \
       --opt "com.docker.network.driver.mtu"="1500" \
       docker_gwbridge
systemctl restart docker.service docker.socket
```

Création d'un nouveau réseau de Backend dans Docker&nbsp;:

```bash
docker network create \
       --driver bridge \
       --subnet=${IP_BACK_SUBNET} \
       --ip-range=${IP_BACK_RANGE} \
       --gateway=${IP_BACK} \
       --internal=true \
       --opt "com.docker.network.bridge.default_bridge"="false" \
       --opt "com.docker.network.bridge.enable_icc"="true" \
       --opt "com.docker.network.bridge.enable_ip_masquerade"="false" \
       --opt "com.docker.network.bridge.name"="${BR_BACK}" \
       --opt "com.docker.network.driver.mtu"="1500" \
       back
```

## Installation de Docker-Compose

```bash
curl --location --url "https://github.com/docker/compose/releases/download/$(curl --head --silent --url 'https://github.com/docker/compose/releases/latest' | grep -oP '^(?:Location).+$' | awk -F'/' '{print $(NF)}' | grep -oP '([0-9]\.?){2,}')/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
cp /etc/fstab{,.bak}
sed -i -r 's@(/tmp.*)noexec@\1exec@' /etc/fstab
mount /tmp -o remount,exec
chmod +x /usr/local/bin/docker-compose
docker-compose version
```
