# Mise en place d'un runner pour GitLab-CI

Téléchargement du GitLab-Runner&nbsp;:

```bash
git clone https://gitlab.miletrie.chl/docker/gitlab-runner /opt/gitlab-runner/
pushd /opt/gitlab-runner/
perl -p -i -e 's#\r##' $(git ls-files)
```

Génération de configurations&nbsp;:

 - Modifier le fichier .env pour correspondre à l'infrastructure utilisée&nbsp;;
 - Génération de configurations pour GitLab-Runner&nbsp;:

```bash
./install.sh
```

Exécution du GitLab-Runner&nbsp;:

```bash
docker-compose up -d
docker-compose logs --follow
```
